#!/bin/bash
DOCKERCOMPOSE="docker compose"
res=`which docker-compose`
if [ -n "$res" ]; then
	DOCKERCOMPOSE="docker-compose"
fi

mandantFile=${1//".env"/""}
if [ -z "$mandantFile" ]; then
	# Siehe https://linuxize.com/post/bash-redirect-stderr-stdout/
	echo "Bitte Namen des Mandanten angeben" 1>&2
	echo "USAGE: $0 <name of mandant file in env/>" 1>&2
	exit 1
fi

projectPath="$PWD"
builderPath="$PWD/builders"
envPath="$PWD/env"
mandantEnvPath="$envPath/$mandantFile.env"
triggersToExecute=( "pre-container-up.sh" ) # Deklaration einer Liste an Strings. Jeder einzelne String repräsentiert den Namen eines Skripts innerhalb eines Builder-Verzeichnisses (Liste aller in Frage kommender Skripts)

source "$mandantEnvPath"

hosts=${hosts//";"//" "}
hosts=( $hosts )

cd "$builderPath"

# Iteriere durch alle Builders (jeder Builder hat einen eigenen Ordner)
#                 Gebe alle Ordner im aktuellen Verzeichnis zurück
#   $folderName ist die Iterationsvariable. Pro Durchlauf enthält sie den Namen eines einzelnen Ordners
for folderName in `ls -d */`; do
	if [ "$folderName" = "lib/" ]; then # ignoriere den 'lib/' Ordner
		continue;
	fi
	
	cd "$folderName" # z.B. 'tilemaker/'
	
	# Iteriere durch die Liste aller in Frage kommender Skripts
	for scriptName in "${triggersToExecute[@]}"; do
		# Prüfe mit '-f', ob Skript mit dem Namen in der Variable $scriptName vorhanden ist
		if [ -f "$scriptName" ]; then
			(
				source "$mandantEnvPath" ;
				source "$projectPath/lib/utils.sh" ;
				source "$scriptName" ;
			) # und wenn dem so ist, dann binde diesen ein, damit darinliegender Code ausgeführt wird
		fi
	done

	$DOCKERCOMPOSE --env-file "$mandantEnvPath" up

	servicename=$(<servicename.prod)
	for host in "${hosts[@]}"; do
		ssh -t manager@$host "mkdir -p '/srv/projekt-produktion/services/$servicename/data/$mandant'"
		rsync -r "./data/output/$mandant/" "manager@$host:/srv/projekt-produktion/services/$servicename/data/$mandant"
		ssh -t manager@$host "cd /srv/projekt-produktion/services/$servicename ; ./restart.sh \"../../env/$mandantFile.env\";"
	done

	cd "$builderPath"
done
