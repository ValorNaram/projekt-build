#!/bin/bash

secondsAYearHas="31536000" # https://www.ecosia.org/search?q=one%20year%20in%20seconds
secondsAMonthHas="2628000" # https://www.ecosia.org/search?q=1%20month%20in%20seconds
secondsAWeekHas="604800" # https://www.ecosia.org/search?q=1%20week%20in%20seconds
secondsADayHas="86400" # https://www.ecosia.org/search?q=1%20day%20in%20seconds

isResourceTooOld() {
	local lastModification=`stat -c %Y "$1"` ## Erhalte Zeitpunkt der letzten Änderung der Datei/des Ordners '$1' in UNIX Epoch
	local today=`date +%s` # Erhalte aktuelle Zeit in UNIX Epoch
	
	local diffTodayAndLastModification=$((today-lastModification))
	
	# Berechne, ob Wert der Differenz größer als $2 in UNIX Epoch
	if (( $diffTodayAndLastModification >= $2 )); then
		true
	else
		false
	fi
}
