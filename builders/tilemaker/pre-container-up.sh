#!/bin/bash
# Lade benötigte Resourcen für alle Builds mit 'tilemaker'

waterPolygons() {
	cd resources/ # Wechsle Arbeitsverzeichnis
	# Prüfe, ob Resource 'coastline' älter als ein Jahr ist
	if `isResourceTooOld "coastline" "$secondsAYearHas"`; then
		echo "Coastline data too old, updating it ..."
		if [ -d "coastline" ]; then
			rm -R coastline # Lösche alten 'coastline' Ordner
		fi
	
		curl https://osmdata.openstreetmap.de/download/water-polygons-split-4326.zip >water-polygons.zip # Lade water-polygons.zip herunter und speichere diese
		unzip water-polygons.zip

		## Da wir nicht den genauen Namen des Ordners kennen, der durch unzip entpackt wurde sondern nur den ungefähren Namen, müssen wir den Namen auflösen
		##          Liste den Inhalt des Arbeitsverzeichnisses auf
		##               Gebe nur zurück was den String "water-polygons-split" enthält
		nameOfFolder=`ls | grep "water-polygons-split"`

		## Benenne den gefundenen Namen in 'coastline' um
		mv "$nameOfFolder" "coastline" --verbose

		## Lösche die ZIP
		rm water-polygons.zip --verbose
		
		echo "Update done!"
	fi
	cd ../
}

regionFromOSM() {
	local foldersToCreate=( "data/input/$mandant" "data/output/$mandant" )
	for folder in "${foldersToCreate[@]}"; do
		# Wenn Ordnerpfad in Variable $folder nicht existiert
		if ! [ -d "$folder" ]; then
			mkdir -p "$folder" --verbose # dann erstelle diesen und alle benötigten darüber
		fi
	done
	cd data/input/$mandant
	echo "Downloading '$region' ..."
#	curl "https://download.geofabrik.de/$region-latest.osm.pbf">input.osm.pbf
	wget "https://download.geofabrik.de/$region-latest.osm.pbf" -O input.osm.pbf
	echo "Done"
	cd ../../../
}

## 1. Lade benötigte Wasserflächen (z.B. Nordsee, Atlantik, Ostsee), deren Geometrien sich nicht oft verändern und somit nicht jedesmal heruntergeladen werden müssen
waterPolygons

## 2. Lade Daten der gewünschten Region aus OSM herunter
### region = "europe/germany/schleswig-holstein"
### Wenn Variable $region über die Shell oder über dieses Skript gesetzt wurde
if [ -n "$region" ]; then
	regionFromOSM # <-- dann führe diese Methode aus
else
	echo "Bitte spezifiziere Variable 'region' in einer Umgebungsdatei"
	echo "z.B. region=\"europe/germany/schleswig-holstein\""
	exit 1
fi
