#!/bin/bash
mandantFile=${1//".env"/""}
if [ -z "$mandantFile" ]; then
	# Siehe https://linuxize.com/post/bash-redirect-stderr-stdout/
	echo "Bitte Namen des Mandanten angeben" 1>&2
	echo "USAGE: $0 <name of mandant file in env>" 1>&2
	exit 1
fi

envPath="$PWD/env"
mandantEnvPath="$envPath/$mandantFile.env"

source "$mandantEnvPath"

hosts=${hosts//";"//" "}
hosts=( $hosts )

key=""
if [ -d "$HOME/.ssh" ]; then
	echo "$HOME/.ssh/ existing, will force not to update key"
	key=`ls ~/.ssh | grep "\.pub" | head -1`
fi
if [ -z "$key" ]; then
	echo "generate SSH key ..."
	ssh-keygen -t ecdsa
fi

key=`ls ~/.ssh | grep "\.pub" | head -1`

for host in "${hosts[@]}"; do
	echo "copying '$key' to '$host' (you will be prompted to enter the password probably) ..."
	ssh-copy-id -i "$HOME/.ssh/$key" manager@$host
done
