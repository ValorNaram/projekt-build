#!/bin/bash
mandantFile=${1//".env"/""}
if [ -z "$mandantFile" ]; then
	# Siehe https://linuxize.com/post/bash-redirect-stderr-stdout/
	echo "Bitte Namen des Mandanten angeben" 1>&2
	echo "USAGE: $0 <name of mandant file in env/>" 1>&2
	exit 1
fi

envPath="$PWD/env"
mandantEnvPath="$envPath/$mandantFile.env"

source "$mandantEnvPath"

echo "creating systemd system service file ..."
servicefilepath="/etc/systemd/system/$mandantFile.service"
if [ -f "$servicefilepath" ]; then
	echo "  already exists, won't overwrite!"
else
	echo "[Unit]
Description=run build pipeline for '$mandantFile'

[Service]
Type=simple
WorkingDirectory=$PWD
ExecStart=./build.sh '$mandantFile'
" | sudo tee "$servicefilepath"
sudo systemctl disable "$mandantFile.service"
fi

echo "creating systemd system timer file ..."
timerfilepath="/etc/systemd/system/$mandantFile.timer"
if [ -f "$timerfilepath" ]; then
	echo " already exists, won't overwrite!"
else
	echo "[Unit]
Description=timer to build '$mandantFile'

[Timer]
OnCalendar=$(date --date="$buildEvery" +"%Y-%m-%d") 00:00:00
OnUnitActiveSec=$buildEvery
Persistent=true

[Install] 
WantedBy=basic.target"| sudo tee "$timerfilepath"
sudo systemctl enable "$mandantFile.timer"
fi

echo "activating systemd system timer ..."
sudo systemctl daemon-reload
